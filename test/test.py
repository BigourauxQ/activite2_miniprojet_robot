""" code de test """

#!/usr/bin/ev python
# coding: utf-8

import unittest
import logging
from Map.grid import Grid



##TESTCODE##
class Test(unittest.TestCase):
    """ class de test pour la classe Grid"""

    def setUp(self):
        """Executed before every test case"""
        self.grid = Grid()
        # self.Keycontrol = Keycontrol_robot()

    def test_up(self):
        """ test commande up_ """
        result = self.grid.up_()
        self.assertEqual(result, False)
        logging.info("Deplacement haut non valide")

    def test_down(self):
        """test commande down """
        result = self.grid.down()
        self.assertEqual(result, True)
        logging.info("Deplacement bas valide")
        self.grid.down()
        result = self.grid.down()
        self.assertEqual(result, False)

    def test_right(self):
        """ test commande right """
        result = self.grid.right()
        self.assertEqual(result, True)
        logging.info("Deplacement droit valide")
        result = self.grid.right()
        result = self.grid.right()
        self.assertEqual(result, False)

    def test_left(self):
        """test commande left """
        result = self.grid.left()
        self.assertEqual(result, False)
        logging.info("Deplacement gauche valide")
        result = self.grid.right()
        result = self.grid.left()
        self.assertEqual(result, True)


if __name__ == "__main__":
    unittest.main()
    