""" code de test """

#!/usr/bin/ev python
# coding: utf-8

import unittest
from Map.keycontrol import Keycontrolrobot


##TESTCODE##
class Test(unittest.TestCase):
    """ class de test pour la classe Grid"""

    def setUp(self):
        """Executed before every test case"""
        self.keycontrol = Keycontrolrobot()

    def test_z(self):
        """ test appuie sur z  """
        result = self.keycontrol.robot_control("z")
        self.assertEqual(result, "z")

    def test_q(self):
        """ test appuie sur q  """
        result = self.keycontrol.robot_control("q")
        self.assertEqual(result, "q")

    def test_s(self):
        """ test appuie sur s  """
        result = self.keycontrol.robot_control("s")
        self.assertEqual(result, "s")

    def test_d(self):
        """ test appuie sur d  """
        result = self.keycontrol.robot_control("d")
        self.assertEqual(result, "d")

    def test_y(self):
        """ test appuie sur y  """
        result = self.keycontrol.robot_control("y")
        self.assertEqual(result, "none")

    def test_n(self):
        """ test appuie sur n  """
        result = self.keycontrol.robot_control("n")
        self.assertEqual(result, "none")


if __name__ == "__main__":
    unittest.main()
