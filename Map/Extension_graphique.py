import turtle
from Grid import Grid
from random import randint
M = Grid(3,3)
M.prints()
longueur_cote = range(1,M.nb_lines)
largeur_cote = range(1,M.nb_colums)

LONGUEUR = 350*len(longueur_cote)
LARGEUR = 350*len(largeur_cote)
turtle.setup(LONGUEUR, LARGEUR)
turtle.shape("square")

def tracer_carre(x,y):
  turtle.up()
  turtle.goto(x,y)
  turtle.setheading(0)
  turtle.down()
  turtle.fillcolor("red")
  turtle.begin_fill()  #Précise le début du remplissage
  for i in range(4):
      turtle.forward(350)  #Côté
      turtle.left(90)  #Angle
  turtle.end_fill()
  turtle.setheading(10)
  turtle.pendown()

def appui_sur_p():
  print("fin")
  turtle.bye()

def appui_quelconque():
  print("Touche pressée !")

def affichage_robot():
  turtle.showturtle() #Ajoute l'image en fond

def sup_robot():
  turtle.hideturtle()

def setup():
  i = 1
  j = 1
  y = -LARGEUR
  while (i <= len(longueur_cote)+1):
    j = 1
    x = -LONGUEUR
    while (j <= len(largeur_cote)+1):
      tracer_carre(x,y)
      x = x + 350
      j = j + 1
    y = y + 350
    i = i + 1
  
def go_up():
  turtle.forward(350)

def go_left():
  turtle.left(90)

def go_down():
  turtle.right(90)

def go_right():
  turtle.backward(350)

def maz():
  turtle.listen()
  turtle.onkeypress(appui_quelconque)  #Toutes les touches libres sont associées à appui_quelconque
  turtle.onkeypress(appui_sur_p, "p")  #La touche A est désormais associée à appui_sur_a
  turtle.onkeypress(affichage_robot, "r")
  turtle.onkeypress(sup_robot, "c")
  turtle.onkeypress(go_up, "z")
  turtle.onkeypress(go_left, "q")
  turtle.onkeypress(go_right, "s")
  turtle.onkeypress(go_down, "d")
  turtle.mainloop()

if __name__ == "__main__":
  setup()
  turtle.up()
  turtle.goto(0,0)
  turtle.setheading(0)
  maz()