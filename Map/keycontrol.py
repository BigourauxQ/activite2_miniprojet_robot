""" code permettant de controler le robot avec le clavier """

#!/usr/bin/ev python
# coding: utf-8

from Map.grid import Grid
import getch


class Keycontrolrobot(Grid):  # keycontrol herite de Grid
    """ class permettant de controler le robot avec le clavier """

    def __init__(self, x=5, y=5):
        """ fonction d'initialisation"""
        super(Keycontrolrobot, self).__init__(x, y)

    def robot_control(self, keyboard):
        """ fonction de controle du robot """
        if keyboard == "z":  # si z est appuye, va en haut

            self.up_()
            self.prints()

            return "z"

        if keyboard == "q":  # si q est appuye, va a gauche

            self.left()
            self.prints()

            return "q"

        if keyboard == "d":  # si d est appuye, va a droite

            self.right()
            self.prints()

            return "d"

        if keyboard == "s":  # si s est appuye, va en bas

            self.down()
            self.prints()

            return "s"

        return "none"


# fonction de teste
if __name__ == "__main__":

    ROBOT = Keycontrolrobot(5, 5)
    while True:
        KEYBOARD = getch.getch()
        ROBOT.robot_control(KEYBOARD)
