""" code creant une matrice dans laquelle un robot represente par un  ce deplace """

#!/usr/bin/env python
# coding: utf-8

from __future__ import print_function
import copy


class Grid(object):
    """ class pemettant de cree un robot dans un environnement """

    def __init__(self, nb_lines=3, nb_colums=3):
        """Fonction d'initisalisation"""
        self.nb_lines = nb_lines
        self.nb_colums = nb_colums
        self._grid = []
        ligne = []  # tableau  contennant une ligne
        while (
                len(ligne) < self.nb_colums
        ):  # on remplie la ligne selon le nombre de colonne
            ligne.append(0)
        while (
                len(self._grid) < self.nb_lines
        ):  # on remplie la matrice selon le nombre de ligne
            lignecopy = copy.deepcopy(
                ligne
            )  # on faite des clones pour eviter d'avoir la meme ligne partout
            self._grid.append(lignecopy)

        self._index = [0, 0]  # coord x et y
        self._grid[0][0] = 1

    def up_(self):
        """deplace le robot en haut  si possible"""

        if self._index[1] + 1 <= 0:  # on verifie que le deplacement soit valide
            self.mise_a_zero()  # si oui on met l'ancienne position a zero
            self._index[1] += 1  # on modifie les coordonnees du robot
            self.actualisegrid()  # on place le robot a son nouvel emplacement
            return True  # on retourne True pour le code de test

        print(
            "mouvement invalide"
        )  # on affiche que le robot ne peut aller dans cette direction
        return False
        # les focntions down, left, right on un fonctionnement similaire

    def down(self):
        """deplace le robot en bas si possible"""
        if self._index[1] - 1 > -self.nb_lines:
            self.mise_a_zero()
            self._index[1] = self._index[1] - 1
            self.actualisegrid()
            return True

        print("mouvement invalide")
        return False

    def left(self):
        """deplace le robot a droite si possible"""
        if self._index[0] - 1 >= 0:
            self.mise_a_zero()
            self._index[0] -= 1
            self.actualisegrid()
            return True

        print("mouvement invalide")
        return False

    def right(self):
        """deplace le robot a droite si possible"""

        if self._index[0] + 1 < self.nb_colums:
            self.mise_a_zero()
            self._index[0] += 1
            self.actualisegrid()
            return True

        print("mouvement invalide")
        return False

    def prints(self):
        """affiche dans le terminale la matrice avec la position du robot"""
        for i in range(
                self.nb_lines
        ):  # on affiche une ligne, saut de ligne, une seconde ligne ...
            print(self._grid[i])
        print("                       ")
        return self._grid

    def actualisegrid(self):
        """place le robot a sa nouvelle position"""

        self._grid[-self._index[1]][self._index[0]] = 1

    def mise_a_zero(self):
        """supprime le robot de son ancienne position"""
        self._grid[-self._index[1]][self._index[0]] = 0


if __name__ == "__main__":

    ROBOT = Grid()
    ROBOT.prints()
    ROBOT.down()
    ROBOT.prints()
