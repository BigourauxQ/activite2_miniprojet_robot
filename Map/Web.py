from flask import Flask, render_template, request
#from Keycontrol import Keycontrol_robot
from Grid import Grid

app = Flask(__name__)

class Keycontrol_robot:
    def __init__(self,x = 5, y = 5):
        self.move = Grid(x,y)


    def robot_control_right(self):

        self.move.right()
        self.move.prints()
        return
       

    def robot_control_down(self):

        self.move.down()
        self.move.prints()
        return

    def robot_control_up(self):

        self.move.up()
        self.move.prints()
        return
        

    def robot_control_left(self):

        self.move.left()
        self.move.prints()
        return
       
  

robot = Keycontrol_robot(8,8)
@app.route('/')
def index():
  return render_template('index.html')

@app.route('/index.html',methods = ['GET'])
def resultat():

    options = request.args.to_dict()
    if options.get('right') == 'Right':
        
        robot.robot_control_right()
        grid1 = robot.move._grid[0]
        grid2 = robot.move._grid[1]
        grid3 = robot.move._grid[2]
        grid4 = robot.move._grid[3]
        grid5 = robot.move._grid[4]
        return render_template("index.html", grid1=grid1,grid2=grid2,grid3=grid3,grid4=grid4,grid5=grid5)
    elif options.get('down') == 'Down':
        robot.robot_control_down()
        grid1 = robot.move._grid[0]
        grid2 = robot.move._grid[1]
        grid3 = robot.move._grid[2]
        grid4 = robot.move._grid[3]
        grid5 = robot.move._grid[4]
        return render_template("index.html", grid1=grid1,grid2=grid2,grid3=grid3,grid4=grid4,grid5=grid5)
    elif options.get('up') == 'Up':
        robot.robot_control_up()
        grid1 = robot.move._grid[0]
        grid2 = robot.move._grid[1]
        grid3 = robot.move._grid[2]
        grid4 = robot.move._grid[3]
        grid5 = robot.move._grid[4]
        return render_template("index.html", grid1=grid1,grid2=grid2,grid3=grid3,grid4=grid4,grid5=grid5)
    elif options.get('left') == 'Left':
        robot.robot_control_left()
        grid1 = robot.move._grid[0]
        grid2 = robot.move._grid[1]
        grid3 = robot.move._grid[2]
        grid4 = robot.move._grid[3]
        grid5 = robot.move._grid[4]
        return render_template("index.html", grid1=grid1,grid2=grid2,grid3=grid3,grid4=grid4,grid5=grid5)
    

if __name__ == '__main__':
    app.run(debug=True)
