# Activité2_Miniprojet_Robot

Second project

Le but du projet est d'afficher une matrice dont les lignes et les colonnes sont renseignées par l'utilisateur.

En rentrant cette ligne de commande : G1 = Grid(5, 2), une matrice G1 est généré contennant 5 lignes et 2 colonnes.

Pour afficher la matrice l'utilisateur doit rentrer la commande G1.prints(). Elle est affiché de la manière suivante :

[1, 0]
[0, 0]
[0, 0]
[0, 0]
[0, 0]

Le 1 correspond à la position initial du robot, il se trouve donc en [0;0].

Pour se déplacer dans la matrice, l'utilisateur entre une des commandes suivantes : G1.up_(), G1.down(), G1.left() ou G1.right()

A chaque entrée de code, le robot se déplacera ou non sur les cases adjacentes. Tant que le robot ne sort pas de la zone de la matrice, celui-ci peut se déplacer en suivant l'ordre indiqué.

Pour afficher la position final du robot il faut rentrer une nouvelle fois la commande G1.prints().

Lors de l'appelle de l'une des 4 fonctions de déplacement, et si le déplacement est autorisé,le robot se déplace.
Nous avons mis en oeuvre 2 méthodes qui permettent d'actualiser les valeurs dans la matrice. Une mise à zéro qui permet de supprimer l'ancienne position du robot et une méthode d'actualisation pour afficher la nouvelle position du robot dans la matrice.

une classe du nom de keycontrolrobot robots permet de controler les déplacements du robots avec le clavier, z == up_() , q == left(), s == down(), d == right() 
