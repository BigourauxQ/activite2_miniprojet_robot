from setuptools import setup
import setuptools

setup(
    name = 'MAP_AdmSys',
    version = '0.0.1',
    author = "BIGOURAUX Quentin THIRY Kevin POISSON Barthelemy",
    packages = ['Map'],
    description = 'Map est un package qui permet de deplacer un robot dans une matrice',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    license = 'GNU GPLv3',
    python_requires = '>=2',
)
